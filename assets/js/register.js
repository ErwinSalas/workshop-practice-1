

function registerUser() {
   firebase.initializeApp(config);
   db = firebase.database();
  
  var name = document.getElementById("name");
  var email = document.getElementById("email");
  var pass = document.getElementById("pass");
  var image = document.getElementById("img");

  if (!name.value || !email.value || !pass.value){
    alert("Debes llenar el formulario primero");
    return null;
  } 

  var id = Date.now();

  db.ref("users/" + id).set({
    name: name.value,
    email: email.value,
    pass: pass.value,
    image:image.value
  });
}
