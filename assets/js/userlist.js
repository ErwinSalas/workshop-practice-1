window.onload = function() {
  getUsers();
};

function getUsers() {
  firebase.initializeApp(config);
  db = firebase.database();
  usersReff = db.ref("users/");
  var container = document.getElementById("contenedor");
  usersReff.on(
    "value",
    function(snapshot) {
      var cont=0;
      snapshot.forEach(function(childSnapshot) {
        // key will be 'ada' the first time and 'alan' the second time
        var key = childSnapshot.key;
        // childData will be the actual contents of the child
        var childData = childSnapshot.val();
        console.log(childData);

       
        var card =
        
        "<div class='col s3 black-text center-align'>" +
          "<div class='card'>" +
                "<div class='card-image waves-effect waves-block waves-light'>" +
                    "<img class='activator' src='" +
                            childData.image +
                    "' width='80' height='200'>" +
                "</div>" +
                '<span class="card-title">'+childData.name+'</span>' +
                "<div class='card-content'>" +
                    "<span class='card-title grey-text text-darken-4'>" +
                    
                    "</span>" +
                    "<p> Email:" +
                    childData.email +
                    "</p>" +
                   
                "</div>" +
            "</div>" +
        "</div>";

        container.innerHTML += card;
      });
    },
    function(errorObject) {
      console.log("The read failed: " + errorObject.code);
    }
  );
}
